# Restaurant data management

We have obtained a (fake) data set from a restaurant containing the customer ID, the time when the customer came (either for lunch or for dinner) and the cost of the first, second and third course. Every course consists of a meal and some drinks. In this case the food has a fixed cost (see the menu below) while the drinks are a random number that is added (let�s just say the restaurant has a very wide range of drinks). 




## Running the tests

[Part 1]  Run the file expl_data.py in the folder Code

[Part 2]  Enter in the file Part2.py, select all the lines, and then right click, choose "Run file in Pyrhon console"

[Part 3]  Run the part3.py

[Part 4]  Run the part4MainMethod.py, then the simulation result will be in the folder Data. Part4.py and part4ClientType.py are the preparation for the main method.

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

