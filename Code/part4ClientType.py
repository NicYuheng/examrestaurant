import numpy
import pandas as pd
from random import choices
from random import randrange
from random import choices
import random
Starter_meal_menu = ['Soup','Tomato','Oyeter','Nothing']
Mains_menu = ['Salad','Spaghetti','Steak','Lobster','Nothing']
Desserts_menu =  ['Ice cream','Pie','Nothing']

Starter_meal = {
    'Soup': 3.0,
    'Tomato': 15.0,
    'Oyeter': 20.0,
    'Nothing':0.0,
}

Mains_meal = {
    'Salad': 9.0,
    'Spaghetti': 20.0,
    'Steak': 25.0,
    'Lobster': 40.0,
    'Nothing': 0.0,
}

Desserts = {
    'Ice cream': 15.0,
    'Pie': 10.0,
    'Nothing': 0.0,
}


#First second third course drinks ranges, it's from part4.py, we just copied to here
price_range_drink_first_course = [random.uniform(0.0,0.667),random.uniform(1.333, 2.0),random.uniform(0.667, 1.333)]
price_range_drink_second_course = [random.uniform(0.0, 4.666),random.uniform(9.332, 13.999),random.uniform(4.666, 9.332)]
price_range_drink_third_course = [random.uniform(0.0, 0.667),random.uniform(1.333, 2.0),random.uniform(0.667, 1.333)]

def getFromDicToList(dict):
    listProbaPerCourse=[]
    for key,value in dict.items():
        listProbaPerCourse.append(value)
    return listProbaPerCourse

def listToString(list):
    return str(list[0])

def listToStringToFloat(list):
    a = str(list[0])
    b = float(a)
    c = "{:.1f}".format(b)
    return c

def test_All_Terms_Zero(list):
    for value in list:
        if value != 0.0:
            return False
    return True

def serach_Price_Of_Meal(menu,name):
    for key,value in menu.items():
        if key==name:
            return value





#Create the parent class type, most of these attribus are needed to create the simulation table
class Client():
    def __init__(self,type,ID,probaReturn,when):
        self.type = type
        self.ID = ID
        self.probaReturn = probaReturn
        self.when = when
        self.firstCourseMeal = None
        self.secondCourseMeal = None
        self.thirdCourseMeal = None
        self.firstCourseDrinkPrice = None
        self.secondCourseDrinkPrice = None
        self.thirdCourseDrinkPrice = None
        self.total1 = None
        self.total2 = None
        self.total3 = None

    def goToRestaurant(self,probaStarter,probaMain,probaDesserts):
        choiceStarter = choices(Starter_meal_menu, probaStarter)
        choiceMain = choices(Mains_menu, probaMain)
        choiceDessert = choices(Desserts_menu, probaDesserts)
        self.firstCourseMeal = listToString(choiceStarter)
        self.secondCourseMeal = listToString(choiceMain)
        self.thirdCourseMeal = listToString(choiceDessert)

    def takeTheDrink(self,listProbaFirstDrink,listProbaSecondDrink,listProbaThirdDrink):
        choiceStarterDrink = choices(price_range_drink_first_course, listProbaFirstDrink)
        choiceMainDrink = choices(price_range_drink_second_course, listProbaSecondDrink)
        choiceDessertDrink = choices(price_range_drink_third_course, listProbaThirdDrink)
        self.firstCourseDrinkPrice = listToStringToFloat(choiceStarterDrink)
        self.secondCourseDrinkPrice = listToStringToFloat(choiceMainDrink)
        self.thirdCourseDrinkPrice = listToStringToFloat(choiceDessertDrink)

    def calculateTotal(self):
        self.total1 = serach_Price_Of_Meal(Starter_meal,self.firstCourseMeal)+float(self.firstCourseDrinkPrice)
        self.total2 = serach_Price_Of_Meal(Mains_meal, self.secondCourseMeal) + float(self.secondCourseDrinkPrice)
        self.total3 = serach_Price_Of_Meal(Desserts,self.thirdCourseMeal)+float(self.thirdCourseDrinkPrice)

#Inherit from the parent class, all the attributs and method
class Onetime(Client):
    def __init__(self,ID,when):
        self.type = 'Onetime'
        self.ID = ID
        self.probaReturn = 0.0
        self.when = when


class Retirement(Client):
    def __init__(self,ID,times,when):
        self.type = 'Retirement'
        self.ID = ID
        self.probaReturn = 0.9
        self.times  = times
        self.when = when



class Business(Client):
    def __init__(self,ID,times,when):
        self.type = 'Business'
        self.ID = ID
        self.probaReturn = 0.5
        self.times = times
        self.when = when


class Healthy(Client):
    def __init__(self,ID,times,when):
        self.type = 'Healthy'
        self.ID = ID
        self.probaReturn = 0.7
        self.times = times
        self.when = when




