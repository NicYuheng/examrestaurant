from Code.part4ClientType import *


#Tuto in StackOverFlow: Generate random numbers with a given (numerical) distribution

#Simulation with 4 types for 36500 times
population = ['Business','Healthy','Onetime', 'Retirement']
weights = [0.199425, 0.216164, 0.434849,0.149562]
million_samples = choices(population, weights, k=36500)

# print(type(million_samples))
from collections import Counter
# print(Counter(million_samples))


## The probability of a certain type of customer ordering a certain dish.
from Code.part3 import *
List_Proba_BSF = getFromDicToList(Proba_BSF)
List_Proba_BMS = getFromDicToList(Proba_BMS)
List_Proba_BDT = getFromDicToList(Proba_BDT)
List_Proba_OSF = getFromDicToList(Proba_OSF)
List_Proba_OMS = getFromDicToList(Proba_OMS)
List_Proba_ODT = getFromDicToList(Proba_ODT)
List_Proba_RSF = getFromDicToList(Proba_RSF)
List_Proba_RMS = getFromDicToList(Proba_RMS)
List_Proba_RDT = getFromDicToList(Proba_RDT)
List_Proba_HSF = getFromDicToList(Proba_HSF)
List_Proba_HMS = getFromDicToList(Proba_HMS)
List_Proba_HDT = getFromDicToList(Proba_HDT)

#We create these lists to contain the data in order to make our simulation table
listWhen = []
listIDClient = []
listTypeClient = []
listStarterOrdered = []
listMainOrdered = []
listDessertOrdered = []
listStarterDrink = []
listMainDrink =[]
listDessertDrink =[]
listTotal1 = []
listTotal2 = []
listTotal3 = []

typeMenu = ['DINNER', 'LUNCH']
from Code.Part4 import *

#For the ID, we assume
#OneTime client : 1-25000, Business : 25001 - 50000
#Healthy client : 50001 - 75000; Retirement : 75001- 10000
compOne = 0
compBus = 25001
compHea = 50001
compRet = 75001
print()
print("-----Simulation of Part 4 Begin ------")

for i in range(36500):
    randomVariable = random.uniform(0, 1)
    instantObj = None
    if(million_samples[i]=='Onetime'):
        whenMenu = choices(typeMenu, list_Onetime_Time_frequency)  #Simulation for 'LUNCH' or 'DINNER'
        compOne = compOne +1                                    #Incremente the counter
        instantObj = Onetime("{:05d}".format(compOne),str(whenMenu[0]))   #We create the object Onetime(ID,when)
        instantObj.goToRestaurant(List_Proba_OSF,List_Proba_OMS,List_Proba_ODT)   #Take the three course
        instantObj.takeTheDrink(listProbaFirstDrink,listProbaSecondDrink,listProbaThirdDrink)  #Have the drink
        instantObj.calculateTotal()  #For the TOTAL1,TOTAL2,TOTAL3
        #listOneTime.append(instantObj)
    elif(million_samples[i]=='Business'):
        whenMenu = choices(typeMenu, list_Business_Time_frequency)
        if(randomVariable<0.5):            #We need to think about the returning rate
            compBus=compBus+1
            instantObj = Business(compBus,1,str(whenMenu[0]))
        else:
            instantObj = Business(randrange(25000,compBus), randrange(2,300),str(whenMenu[0]))  #Creat the object Business(self,ID,times,when)
        instantObj.goToRestaurant(List_Proba_BSF, List_Proba_BMS, List_Proba_BDT)
        instantObj.takeTheDrink(listProbaFirstDrink, listProbaSecondDrink, listProbaThirdDrink)
        instantObj.calculateTotal()
        #listBusiness.append(instantObj)
    elif(million_samples[i]=='Healthy'):
        whenMenu = choices(typeMenu, list_Healthy_Time_frequency)
        if (randomVariable > 0.7):
            compHea = compHea + 1
            instantObj = Healthy(compBus, 1,str(whenMenu[0]))
        else:
            instantObj = Healthy(randrange(50000, compHea), randrange(2,500),str(whenMenu[0]))
        instantObj.goToRestaurant(List_Proba_HSF, List_Proba_HMS, List_Proba_HDT)
        instantObj.takeTheDrink(listProbaFirstDrink, listProbaSecondDrink, listProbaThirdDrink)
        instantObj.calculateTotal()
        #listHealthy.append(instantObj)
    else:
        whenMenu = choices(typeMenu, list_Retirement_Time_frequency)
        if (randomVariable > 0.9):
            compRet = compRet + 1
            instantObj = Retirement(compRet, 1,str(whenMenu[0]))
        else:
            instantObj = Retirement(randrange(75000, compRet), randrange(2,1000),str(whenMenu[0]))
        instantObj.goToRestaurant(List_Proba_RSF, List_Proba_RMS, List_Proba_RDT)
        instantObj.takeTheDrink(listProbaFirstDrink, listProbaSecondDrink, listProbaThirdDrink)
        instantObj.calculateTotal()
        #listRetirement.append(instantObj)
    #Add in our list already prepared
    listWhen.append(instantObj.when)
    listTypeClient.append(instantObj.type)
    listIDClient.append(instantObj.ID)
    listStarterOrdered.append(instantObj.firstCourseMeal)
    listMainOrdered.append(instantObj.secondCourseMeal)
    listDessertOrdered.append(instantObj.thirdCourseMeal)
    listStarterDrink.append(instantObj.firstCourseDrinkPrice)
    listMainDrink.append(instantObj.secondCourseDrinkPrice)
    listDessertDrink.append(instantObj.thirdCourseDrinkPrice)
    listTotal1.append(instantObj.total1)
    listTotal2.append(instantObj.total2)
    listTotal3.append(instantObj.total3)


MyFinalDataframe = pd.DataFrame({'TIME': listWhen,
                            'CUSTOMERID': listIDClient,
                            'CUSTOMERTYPE': listTypeClient,
                            'COURSE1': listStarterOrdered,
                            'COURSE2' :listMainOrdered,
                            'COURSE3' : listDessertOrdered,
                            'DRINKS1' : listStarterDrink,
                            'DRINKS2' : listMainDrink,
                            'DRINKS3' : listDessertDrink,
                            'TOTAL1'  : listTotal1,
                            'TOTAL2' :  listTotal2,
                            'TOTAL3' : listTotal3
                            })
print(MyFinalDataframe)

#Output in the Simulation_Result.csv
outpathFile = os.path.abspath('../Data/Simulation_Result.csv')
MyFinalDataframe.to_csv(outpathFile)

##########################################################
#  Comparaion between the simualtion and the original data
############################################################

##Type distribution
Types_distribution_Simulation = MyFinalDataframe.groupby('CUSTOMERTYPE').size().to_frame().reset_index()
Types_frequency_Simulation = frequency(Types_distribution_Simulation)
print("The type frenquency of our simulation")
print(Types_frequency_Simulation)
print()
print("The type frenquency of part3.csv")
print(Types_frequency)

##Cost distribution
plt.title("Distribution of the cost in first course in our simulation")
plt.hist(MyFinalDataframe['TOTAL1'], bins=50,rwidth=0.95)
plt.show()
##We can compare with the plot we hade in part1 (explo_data.py), the comments are in the reports

############################################
print()
print("------------------------END--------------------------------")