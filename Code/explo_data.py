import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import os
import numpy as np
import scipy.stats as stats
import seaborn as sns
import numpy as np
import scipy.stats as stats
from scipy.stats import norm
import pylab as pl

# *************************************
#    Read in data of part1.csv
# *************************************

# setting a generic path
outpath = os.path.abspath('../Data/part1.csv')
print("File path of the data file:")
print(outpath)
# reading csv file
df = pd.read_csv(outpath)
print(df)




# *************************************
#    Make a plot of the distribution of the cost in each course
# **************************************

# A plot of the distribution of the cost in first course
plt.title("Distribution of the cost in first course")
plt.hist(df['FIRST_COURSE'], bins=50,rwidth=0.95)
plt.show()
# A plot of the distribution of the cost in second course
plt.title("Distribution of the cost in second course")
plt.hist(df['SECOND_COURSE'], bins=50,rwidth=0.95)
plt.show()
# A plot of the distribution of the cost in third course
plt.title("Distribution of the cost in third course")
plt.hist(df['THIRD_COURSE'], bins=50,rwidth=0.95)
plt.show()

#
# # *************************************
# #    Make a barplot of the cost per course
# # **************************************

# plot between 2 attributes
x_value = list(range(1, 36501))
plt.title("Cost in first course")
y_pos = np.arange(len(x_value))
plt.bar(y_pos,df['FIRST_COURSE'],)
plt.show()
#
plt.title("Cost in second course")
y_pos = np.arange(len(x_value))
plt.bar(y_pos,df['SECOND_COURSE'],)
plt.show()
#
plt.title("Cost in third course")
y_pos = np.arange(len(x_value))
plt.bar(y_pos,df['THIRD_COURSE'],)
plt.show()


# ************************************************
#    Determine the cost of the drinks per course
# ************************************************

# The price of meal in each course
starters = [3, 15, 20]
starters.sort(reverse=True)
mains = [20, 25, 40]
desserts = [15, 10]

mains.sort(reverse=True)
desserts.sort(reverse=True)


# Determine the cost of the drinks per course
# and add the two coloumns containing the price of meal and drink in the datasheet
def cost_drink_per_course(courseLabel, listMeal, labelColMeal, LabelColDrink):
    # Write down the prices of meal and drink corresponding to each purchase, save it in a list.
    meal = []
    drink = []
    for each in df[courseLabel]:
        i = 0
        no_meal_but_drink = False
        while (each <= float(listMeal[i]) and each != float(0) and no_meal_but_drink == False):
            if i == (len(listMeal) - 1):
                no_meal_but_drink = True  # If we already at the last position of the listMeal, it means there are just some drink in the course
            else:
                i = i + 1
        if each == float(0):
            # No order in this course
            meal.append(0.0)
            drink.append(0.0)
        elif no_meal_but_drink == True:
            # There are just some drink in the course
            meal.append(0.0)
            drink.append(each)
        else:
            # There are both meal and drink
            meal.append(listMeal[i])
            drink.append(each - float(listMeal[i]))
    df[labelColMeal] = meal
    df[LabelColDrink] = drink


cost_drink_per_course('FIRST_COURSE', starters, 'FIRST_COURSE_MEAL', 'FIRST_COURSE_DRINK')
cost_drink_per_course('SECOND_COURSE', mains, 'SECOND_COURSE_MEAL', 'SECOND_COURSE_DRINK')
cost_drink_per_course('THIRD_COURSE', desserts, 'THIRD_COURSE_MEAL', 'THIRD_COURSE_DRINK')

# Print the final datasheet, normarlly it has 11 columns
print(df)
outpathFile = os.path.abspath ( '../Data/DATASET3_DISHES.csv' )
df.to_csv(outpathFile)
##df.to_csv(r'C:\Users\xmjus\Desktop\Python Project\Part1Solution.csv')
