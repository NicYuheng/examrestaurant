import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import os

import numpy as np
# *************************************
#    Read in data of DATASET1.csv
# *************************************

# setting a generic path
outpath = os.path.abspath('../Data/DATASET1.csv')
print("File path of the data file:")
print(outpath)
# reading csv file
df = pd.read_csv(outpath)
#print(df)


# setting a generic path
outpath = os.path.abspath('../Data/part3.csv')
print("File path of the data file:")
print(outpath)
# reading csv file
part3 = pd.read_csv(outpath)

# In part3.csv you can find the ID and the actual type. Compare the labels to the actual client type.
# Add a column that gets if the label is correct or wrong
data_set2 = df
guess = []
rows = data_set2.count
for i in range(1, 36002):
    if data_set2.iloc[i, 6] == part3.iloc[i, 1]:
        guess.append('Correct')
    else:
        guess.append('False')
comparision = pd.DataFrame({'Correct/False': guess})
data_set2["Our_Guess"] = comparision
print (data_set2.groupby('Our_Guess').size())
outpathFile = os.path.abspath('../Data/DATASET2.csv')
data_set2.to_csv(outpathFile)
# It is better here we create a dataframe containing only ID + Type + Correct/False


# A plot of the distribution of the client in our restaurant"
plt.title("Distribution of the client in our restaurant")
plt.hist(df['Label_Customer'], bins=50, rwidth=0.95)
plt.show()
print()

# Can you determine the likelihood for each of these clients to get a certain course.
# So answering the question “How likely is this type of client to get a starter, main and dessert?”

print('----------   Question 3 -------------')
col1 = [i for i in range(4)]
col2 = ['Onetime', 'Retirement', 'Business', 'Healthy']

#We want create a table containing these terms: index,client type,
# GET_STARTER number, GET_MAIN number, GET_DESSERT number
MyDataframe = pd.DataFrame({'INDEX': col1,
                            'CLIENT_TYPE': col2,
                            })
get_starter = []
get_main = []
get_dessert = []


def get_number_course(label, course):
    return df[(df['Label_Customer'] == label) &
              (df[course] != 0.0)].shape[0]


# For each client type,how many of them chose to have the starter
get_starter.append(get_number_course('Onetime', 'FIRST_COURSE'))
get_starter.append(get_number_course('Retirement', 'FIRST_COURSE'))
get_starter.append(get_number_course('Business', 'FIRST_COURSE'))
get_starter.append(get_number_course('Healthy', 'FIRST_COURSE'))
# print(get_starter)

# For each client type,how many of them choise to have the main
get_main.append(get_number_course('Onetime', 'SECOND_COURSE'))
get_main.append(get_number_course('Retirement', 'SECOND_COURSE'))
get_main.append(get_number_course('Business', 'SECOND_COURSE'))
get_main.append(get_number_course('Healthy', 'SECOND_COURSE'))
# print(get_main)
# For each client type,how many of them choise to have the dessert
get_dessert.append(get_number_course('Onetime', 'THIRD_COURSE'))
get_dessert.append(get_number_course('Retirement', 'THIRD_COURSE'))
get_dessert.append(get_number_course('Business', 'THIRD_COURSE'))
get_dessert.append(get_number_course('Healthy', 'THIRD_COURSE'))
# print(get_dessert)

MyDataframe['GET_STARTER'] = get_starter
MyDataframe['GET_MAIN'] = get_main
MyDataframe['GET_DESSERT'] = get_dessert
print(MyDataframe)
print()
print()

# Question 4 Can you figure out the probability of a certain type of customer ordering a certain dish.
# For example: a Onetime customer buys ice cream in 30% cases and pie in 70% case (if they have dessert).
print('----------   Question 4 -------------')

outpathFile = os.path.abspath('../Data/DATASET3_DISHES.csv')  # This file is from explo_data.py
dishes_perType = pd.read_csv(outpathFile)
print(dishes_perType)
#Add a coloumn to see the type of client
dishes_perType['Label_Customer'] = data_set2['Label_Customer']
#print(dishes_perType)

# First let us filter by the client who took the meal without drink and their order
#As we just need these info for the moment
client_meals_no_drink = dishes_perType[
    ['CLIENT_ID', 'FIRST_COURSE_MEAL', 'SECOND_COURSE_MEAL', 'THIRD_COURSE_MEAL', 'Label_Customer']]
print()
print()
print("-- Meals no drink --")
print(client_meals_no_drink)

# find in each category, how many of each dish has been taken

# The there menus are stored in dictionnary
Starter_meal = {
    'Soup': 3.0,
    'Tomato': 15.0,
    'Oyeter': 20.0,
    'Nothing':0.0,
}

Mains_meal = {
    'Salad': 9.0,
    'Spaghetti': 20.0,
    'Steak': 25.0,
    'Lobster': 40.0,
    'Nothing': 0.0,
}

Desserts = {
    'Ice cream': 15.0,
    'Pie': 10.0,
    'Nothing': 0.0,
}


# Calculate the probability of a certain type of customer ordering a certain dish.
def calculate_Percentage(type_Client, course_Meal, title_Course_Meal):
    probaCusDish={}
    print('In the ' + title_Course_Meal + "  they ordered :")
    # Seperating each type of clients
    unique_Type_Client = client_meals_no_drink[client_meals_no_drink['Label_Customer'] == type_Client]
    total_Number_Have_Course = (unique_Type_Client.shape[0])  #Need this number to calculate probability
    #total_Number_Have_Course = (unique_Type_Client[unique_Type_Client[title_Course_Meal!=0.0]].shape[0])
    for key, value in course_Meal.items():
        numberEachCourse = unique_Type_Client[unique_Type_Client[title_Course_Meal] == value].shape[0]
        # print(numberEachCourse)
        if total_Number_Have_Course != 0:  #Avoid the case when divid by 0
            percentage_Each_Course = numberEachCourse / total_Number_Have_Course  # Calculate percentage
        else:
            percentage_Each_Course = 0.0
        probaCusDish[key]= percentage_Each_Course
        #print(key + ": " + str("{:.0%}".format(percentage_Each_Course)))  # Have the percentage format
    print(probaCusDish)
    return probaCusDish


print('The distribution of dishes, per course, for -- Business -- client')
Proba_BSF=calculate_Percentage('Business', Starter_meal, 'FIRST_COURSE_MEAL')
Proba_BMS=calculate_Percentage('Business', Mains_meal, 'SECOND_COURSE_MEAL')
Proba_BDT=calculate_Percentage('Business', Desserts, 'THIRD_COURSE_MEAL')
print()

print('The distribution of dishes, per course, for -- Onetime -- client')
Proba_OSF=calculate_Percentage('Onetime', Starter_meal, 'FIRST_COURSE_MEAL')
Proba_OMS=calculate_Percentage('Onetime', Mains_meal, 'SECOND_COURSE_MEAL')
Proba_ODT=calculate_Percentage('Onetime', Desserts, 'THIRD_COURSE_MEAL')
print()

print('The distribution of dishes, per course, for -- Retirement -- client')
Proba_RSF=calculate_Percentage('Retirement', Starter_meal, 'FIRST_COURSE_MEAL')
Proba_RMS=calculate_Percentage('Retirement', Mains_meal, 'SECOND_COURSE_MEAL')
Proba_RDT=calculate_Percentage('Retirement', Desserts, 'THIRD_COURSE_MEAL')
print()

print('The distribution of dishes, per course, for -- Healthy -- client')
Proba_HSF=calculate_Percentage('Healthy', Starter_meal, 'FIRST_COURSE_MEAL')
Proba_HMS=calculate_Percentage('Healthy', Mains_meal, 'SECOND_COURSE_MEAL')
Proba_HDT=calculate_Percentage('Healthy', Desserts, 'THIRD_COURSE_MEAL')
print()

print('---------- End of Question 4 & Question 5-------------')
print()

# Question 5 Can you determine the distribution of the cost of the drinks per course?
# You should have that in a column from part 1.
print('----------   Question 6 -------------')

print(dishes_perType)
print(dishes_perType['FIRST_COURSE_DRINK'])

# A plot of the distribution of the drink cost in first course
plt.title("Distribution of the drink cost in first course")
plt.hist(dishes_perType['FIRST_COURSE_DRINK'][(dishes_perType['FIRST_COURSE_DRINK'] != 0)], bins=20, rwidth=0.95)


plt.show()

# A plot of the distribution of the drink cost in second course
plt.title("Distribution of the drink cost in second course")
plt.hist(dishes_perType['SECOND_COURSE_DRINK'][(dishes_perType['SECOND_COURSE_DRINK'] != 0)], bins=20, rwidth=0.95)
plt.show()

# A plot of the distribution of the drink cost in third course
plt.title("Distribution of the drink cost in third course")
plt.hist(dishes_perType['THIRD_COURSE_DRINK'][(dishes_perType['THIRD_COURSE_DRINK'] != 0)], bins=20, rwidth=0.95)
plt.show()


print("---------   END OF part3.py  -----------")