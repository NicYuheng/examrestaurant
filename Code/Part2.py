import pandas as pd
import os
import pandas as pd
import numpy as np

import matplotlib.pyplot as plt

# setting a generic path
outpath = os.path.abspath ( '../Data/part1.csv' )
print ( "File path of the data file:" )
print ( outpath )
# reading csv file
data_restaurant = pd.read_csv ( outpath )


# Prepare Data - We need only the 3 columns of the prices.


# 1st step: Drop the 2 columns: ID and time
data_restaurant.pop ( 'CLIENT_ID' )
data_restaurant.pop ( 'TIME' )

# Second Step: switch the data frame into a list / use .values.tolist()
orders = data_restaurant.values.tolist ()
orders = np.array ( orders )

# K-means for clustering the data
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.cluster import KMeans
from sklearn import datasets

np.random.seed ( 5 )
X = orders
name = 'k_means_customers_3'
est = KMeans ( n_clusters=4 )
fignum = 1
titles = ['IDs: 4 clusters by group']
fig = plt.figure ( fignum, figsize=(4, 3) )
ax = Axes3D ( fig, rect=[0, 0, .95, 1], elev=48, azim=134 )
est.fit ( X )
labels = est.labels_
print(labels)



# Merge the labels obtained in the part1.csv,
# add an extra column in the end with the name "Label_Customer"
# then print our result in the form of a table
data_restaurant2 = pd.read_csv ( outpath )
labels_copy = list(map(str,labels))
for index, each_Label in enumerate(labels_copy):
    if each_Label == '0':
        labels_copy[index]='Onetime'
    elif each_Label == '1':
        labels_copy[index] = 'Retirement'
    elif each_Label == '2':
        labels_copy[index] = 'Business'
    else:
        labels_copy[index] = 'Healthy'
data_restaurant2['Label_Customer'] = labels_copy
print(data_restaurant2)
outpathFile = os.path.abspath ( '../Data/DATASET1.csv' )
data_restaurant2.to_csv(outpathFile)
# Plotting the 3D plot
ax.scatter ( X[:, 0], X[:, 1], X[:, 2],
             c=labels.astype ( np.float ), edgecolor='k' )
ax.w_xaxis.set_ticklabels ( [] )
ax.w_yaxis.set_ticklabels ( [] )
ax.w_zaxis.set_ticklabels ( [] )
ax.set_xlabel ( 'First_COURSE' )
ax.set_ylabel ( 'SECOND_COURSE' )
ax.set_zlabel ( 'THIRD_COURSE' )
ax.set_title ( titles[fignum - 1] )
ax.dist = 12
fignum = fignum + 1
fig.show ()



for name, label in [('Onetime', 0),
                    ('Retirement', 1),
                    ('Business', 2), ('Healthy', 3)]:
    ax.text3D ( X[labels == label, 0].mean (),
                X[labels == label, 1].mean (),
                X[labels == label, 2].mean () , name,
                horizontalalignment='center',
                bbox=dict ( alpha=.2, edgecolor='w', facecolor='w' ) )


# Adding the labels:
# After finding the labels, I exported them into a csv file. Then by comparing each orders purchases and the
# corresponding label(Target) I tried to find patterns and assign each label to a certain category(Business, Onetime,
# Retirement or Healthy
# We know that
# Business, 2: usually come in and are more likely to have the more expensive dishes.
# Healthy, 3: usually go for soups or salads and obviously hardly ever take desserts.
# Retirement, 1 usually take a three-course menu often ending with a nice piece of pie.
# One time, 0 who are passing by for a quick main dish.



# Can you find out any specific characteristics per group?
# Retirement group is the one that has most of the time the 3 courses
# The one time group only has the SECOND COURSE
# The Business group have the most expensive dishes
# The healthy usually do not have the dessert
# These groups can be clearly seen in their position in the 3D plot.
