import os
import pandas as pd
from random import randrange
from random import choices
import random
import numpy as np


outpathFile = os.path.abspath ( '../Data/DATASET2.csv' )
datacustomers = pd.read_csv(outpathFile)
print(datacustomers)
##Type distribution
Types_distribution = datacustomers.groupby('Label_Customer').size().to_frame().reset_index()

##Time distribution Business
Business_data = datacustomers[datacustomers['Label_Customer'] == "Business"]
Business_Time = Business_data.groupby('TIME').size().to_frame().reset_index()


##Time distribution Healthy
Healthy_data = datacustomers[datacustomers['Label_Customer'] == "Healthy"]
Healthy_Time = Healthy_data.groupby('TIME').size().to_frame().reset_index()
print(Healthy_Time)


##Time distribution Retirement
Retirement_data = datacustomers[datacustomers['Label_Customer'] == "Retirement"]
Retirement_Time = Retirement_data.groupby('TIME').size().to_frame().reset_index()
print(Retirement_Time)

##Time distribution Onetime
Onetime_data = datacustomers[datacustomers['Label_Customer'] == "Onetime"]
Onetime_Time = Onetime_data.groupby('TIME').size().to_frame().reset_index()

#Function to calculate the probability inside a dataframe
def frequency (data):
    total = sum(data[0])
    rows = len(data.index)
    freq=[]
    for i in range(0,rows):
        freq.append(data.loc[i,0]/total)
    data['probability'] = freq
    return data

Business_Time_frequency = frequency(Business_Time)
Healthy_Time_frequency = frequency(Healthy_Time)
Retirement_Time_frequency = frequency(Retirement_Time)
Onetime_Time_frequency = frequency(Onetime_Time)



list_Retirement_Time_frequency = Retirement_Time_frequency['probability'].tolist()
list_Onetime_Time_frequency = Onetime_Time_frequency['probability'].tolist()
list_Business_Time_frequency = Business_Time_frequency['probability'].tolist()
list_Healthy_Time_frequency = Healthy_Time_frequency['probability'].tolist()


Types_frequency = frequency(Types_distribution)
##Onetime_Time.sum(axis=0))
print(Types_frequency)

## Now we find the distribution/probability of the cost of the drinks per dish per client?
##Let's first get a data frame for the drinks per type
outpathFile = os.path.abspath ( '../Data/DATASET3_DISHES.csv' )
drinks = pd.read_csv(outpathFile)
#Function to calculate probability
def proba (data):
    total = 36000
    rows = 3
    freq=[]
    for i in range(0,rows):
        freq.append(data.iloc[i,1]/total)
    data['probability'] = freq
    return data

#First course drinks ranges
FIRST_COURSE_DRINK = drinks['FIRST_COURSE_DRINK']
FIRST_COURSE_DRINK= FIRST_COURSE_DRINK.value_counts(bins=3).to_frame().reset_index()
FIRST_COURSE_DRINK = proba(FIRST_COURSE_DRINK)

#Second Course Drinks ranges
SECOND_COURSE_DRINK = drinks['SECOND_COURSE_DRINK']
SECOND_COURSE_DRINK = SECOND_COURSE_DRINK.value_counts(bins=3).to_frame().reset_index()
SECOND_COURSE_DRINK = proba(SECOND_COURSE_DRINK)

#Third Course Drinks ranges
THIRD_COURSE_DRINK = drinks['THIRD_COURSE_DRINK']
THIRD_COURSE_DRINK = THIRD_COURSE_DRINK.value_counts(bins=3).to_frame().reset_index()
THIRD_COURSE_DRINK = proba(THIRD_COURSE_DRINK)

print(FIRST_COURSE_DRINK)
print(SECOND_COURSE_DRINK)
print(THIRD_COURSE_DRINK)

listProbaFirstDrink = FIRST_COURSE_DRINK['probability'].tolist()
listProbaSecondDrink = SECOND_COURSE_DRINK['probability'].tolist()
listProbaThirdDrink = THIRD_COURSE_DRINK['probability'].tolist()



